(function(){

  'use strict';

	angular.module('app.dashboard', [])
  .controller('dashboardController', dashboardController)

	dashboardController.$inject = ['$scope'];

	function dashboardController($scope) {

		let vm = this;

    vm.visibleComponentName = 'sale';

    vm.toggleVisible = function(componentName) {
      vm.visibleComponentName = componentName;
      $scope.$broadcast('componentRefresh', {});
    }
	}

})();
