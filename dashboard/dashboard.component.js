(function () {
	'use strict';

	angular
		.module ('app.dashboard')
		.component ('dashboard', {

		//
		// Usage: <add-edit-author></add-edit-author>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			//template: `<p>dashboard</p>`,
      template:
      `
			<div style="background-color:#efefef;text-align:center;margin-top:5%;margin-bottom:5%;">
				<button ng-click="vm.toggleVisible('author')">author</button>
				<button ng-click="vm.toggleVisible('book')">book</button>
				<button ng-click="vm.toggleVisible('vendor')">vendor</button>
				<button ng-click="vm.toggleVisible('sale')">sales</button>
			</div>

			<add-edit-author visible-component-name="vm.visibleComponentName"></add-edit-author>
			<add-edit-book visible-component-name="vm.visibleComponentName"></add-edit-book>
			<add-edit-vendor visible-component-name="vm.visibleComponentName"></add-edit-vendor>
			<add-edit-sale visible-component-name="vm.visibleComponentName"></add-edit-sale>
      `,
			controller: 'dashboardController',
			controllerAs: 'vm',
			bindToController: true // because the scope is isolated
		})

})();
