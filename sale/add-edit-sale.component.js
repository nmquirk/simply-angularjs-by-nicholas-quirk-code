(function () {
	'use strict';

	angular
		.module ('app.sale')
		.component ('addEditSale', {

		//
		// Usage: <add-edit-sale></add-edit-sale>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName"'>
				<form>
					<fieldset class="flex four">
						<input type="hidden" ng-model="vm.id"/>

						<label>
							<select ng-model="vm.bookTitle">
								<option value="">book</option>
								<option ng-repeat="book in vm.books">{{book.title}}</option>
							</select>
						</label>
						<label>
							<select ng-model="vm.vendorName">
								<option value="">vendor</option>
								<option ng-repeat="vendor in vm.vendors">{{vendor.name}}</option>
							</select>
						</label>

						<label><input type="number" placeholder="price" ng-model="vm.price"/></label>
						<label><input type="number" placeholder="quantity" ng-model="vm.quantity"/></label>
						<label><input type="date" placeholder="date" ng-model="vm.date"/></label>

					</fieldset>
					<button ng-click="vm.save()" class="success">save</button>
				</form>
			</div>
			<view-sales visible-component-name="vm.visibleComponentName"></view-sales>
			`,
			controller: 'addEditSaleController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<' // make this 1-way
			},
			bindToController: true // because the scope is isolated
		})

})();
