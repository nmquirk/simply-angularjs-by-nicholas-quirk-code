(function () {
	'use strict';

	angular
		.module ('app.sale')
		.component ('viewSales', {

		//
		// Usage: <add-edit-author></add-edit-author>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName">
				<table>
					<thead>
						<tr>
							<th>actions</th>
							<th>title</th>
							<th>vendor</th>
							<th>date</th>
	            <th>price</th>
	            <th>quantity</th>
							<th>total</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="sale in vm.sales track by $index">
							<td>
								<button ng-click="vm.editSale(sale)">edit</button>
								<button ng-click="vm.deleteSale(sale)" class="error">delete</button>
							</td>
							<td>{{sale.bookTitle}}</td>
							<td>{{sale.vendorName}}</td>
							<td>{{sale.date | date: 'yyyy-MM-dd'}}</td>
              <td>{{sale.price}}</td>
              <td>{{sale.quantity}}</td>
              <td>{{sale.price * sale.quantity | currency}}</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>grand total:</td>
							<td>{{ vm.grandTotal | currency}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
			`,
			controller: 'viewSalesController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<'
			},
			bindToController: true // because the scope is isolated
		})

})();
