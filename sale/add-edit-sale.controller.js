(function(){

  'use strict';

	angular.module('app.sale')
  .controller('addEditSaleController', addEditSaleController)

	addEditSaleController.$inject = ['$scope', 'dataservice'];

	function addEditSaleController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'sale';

    // function declarations
    vm.onInit = onInit;
    vm.save = save;

    // event handlers
    $scope.$on('editSale', function(event, data) {
      vm.bookTitle = data.bookTitle;
      vm.vendorName = data.vendorName;
      vm.price = data.price;
      vm.quantity = data.quantity;
      vm.date = new Date(data.date);
      vm.id = data.id;
    });

    $scope.$on('componentRefresh', function(event, data) {
      onInit();
    });

    // function implementations
    function save() {
      dataservice.saveSale(
        { 'bookTitle': vm.bookTitle,
          'vendorName' : vm.vendorName,
          'price' : vm.price,
          'quantity' : vm.quantity,
          'date' : vm.date,
          'id' : vm.id
        }
      );

      vm.bookTitle = '';
      vm.vendorName = '';
      vm.price = '';
      vm.quantity = '';
      vm.date = '';
      vm.id = '';

      $scope.$broadcast('refreshSaleList', {});
    }

    function onInit() {
      vm.books = dataservice.getBooks();
      vm.vendors = dataservice.getVendors();
    }

    onInit();
	}

})();
