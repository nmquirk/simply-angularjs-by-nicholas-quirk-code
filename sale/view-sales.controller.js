(function(){

  'use strict';

	angular.module('app.sale')
  .controller('viewSalesController', viewSalesController)

	viewSalesController.$inject = ['$scope', 'dataservice'];

	function viewSalesController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'sale';
    vm.sales = [];
    vm.grandTotal = 0;

    // function declarations
    vm.onInit = onInit;
    vm.editSale = editSale;
    vm.deleteSale = deleteSale;

    // event handlers
    $scope.$on('refreshSaleList', function(event, data) {
      onInit();
    });

    // function implementations
    function editSale(sale) {
      $scope.$emit('editSale', sale);
    }

    function deleteSale(sale) {
      dataservice.deleteSale(sale);
      onInit();
    }

    function onInit() {
      vm.sales = dataservice.getSales();
      vm.grandTotal = 0;
      vm.sales.forEach(function(sale) {
        vm.grandTotal += (sale.price * sale.quantity);
      });
    }

    onInit();
	}

})();
