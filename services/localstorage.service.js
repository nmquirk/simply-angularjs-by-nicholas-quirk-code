(function(){

  'use strict';

  angular
      .module('app.core', [])
      .factory('dataservice', dataservice);

  dataservice.$inject = [];

  function dataservice() {

      return {
          saveAuthor: saveAuthor,
          getAuthors: getAuthors,
          deleteAuthor: deleteAuthor,

          saveBook: saveBook,
          getBooks: getBooks,
          deleteBook: deleteBook,

          saveVendor: saveVendor,
          getVendors: getVendors,
          deleteVendor: deleteVendor,

          saveSale: saveSale,
          getSales: getSales,
          deleteSale: deleteSale
      };

      // HIGHER ORDER FUNCTIONS
      function getRecords(collectionName) {
        if(localStorage.getItem(collectionName)) {
          return JSON.parse(localStorage.getItem(collectionName));
        }
        return {};
      }

      function deleteRecord(collectionName, record) {

        let records = JSON.parse(localStorage.getItem(collectionName));
        let newRecordList = [];

        records.forEach(function(r) {
          if(r.id !== record.id) newRecordList.push(r);
        });

        records = newRecordList;
        localStorage.setItem(collectionName, JSON.stringify(records));

      }

      function saveRecord(collectionName, record, id) {

        if(localStorage.getItem(collectionName) == null) {
          localStorage.setItem(collectionName, '[]');
        }

        if(record.id) {
          // delete the current entry
          // then process to enter a new one.
          deleteRecord(collectionName, record);
        }

        let records = JSON.parse(localStorage.getItem(collectionName));
        record.id = id;

        let isUniqueEntry = true;
        // If you get an error with forEach use a for loop instead.
        records.forEach(function(r) {
          if(r.id === record.id) isUniqueEntry = false;
        });

        if(isUniqueEntry) {
          records.push(record);
          localStorage.setItem(collectionName, JSON.stringify(records));
        }
      }

      // AUTHOR
      function saveAuthor(author) {
        let id = author.firstName+author.lastName;
        saveRecord('authors', author, id);
      }

      function getAuthors() {
        return getRecords('authors');
      }

      function deleteAuthor(author) {
        deleteRecord('authors', author);
      }

      // BOOK
      function saveBook(book) {
        let id = book.title+book.isbn+book.author; //value based key
        saveRecord('books', book, id);
      }

      function getBooks() {
        return getRecords('books');
      }

      function deleteBook(book) {
        deleteRecord('books', book);
      }

      // VENDOR
      function saveVendor(vendor) {
        let id = vendor.name; //value based key
        saveRecord('vendors', vendor, id);
      }

      function getVendors() {
        return getRecords('vendors');
      }

      function deleteVendor(vendor) {
        deleteRecord('vendors', vendor);
      }

      function saveSale(sale) {
        let id = sale.bookTitle+sale.vendorName+sale.price+sale.quantity+sale.date; //value based key
        saveRecord('sales', sale, id);
      }

      function getSales() {
        return getRecords('sales');
      }

      function deleteSale(sale) {
        deleteRecord('sales', sale);
      }

    }

})();
