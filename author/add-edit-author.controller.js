(function(){

  'use strict';

	angular.module('app.author')
  .controller('addEditAuthorController', addEditAuthorController)

	addEditAuthorController.$inject = ['$scope', 'dataservice'];

	function addEditAuthorController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'author';

    // function declarations
    vm.save = save;

    // event handlers
    $scope.$on('editAuthor', function(event, data) {
      vm.firstName = data.firstName;
      vm.lastName = data.lastName;
      vm.id = data.id;
    });

    // function implementations
    function save() {
      dataservice.saveAuthor(
        { 'firstName': vm.firstName,
          'lastName' : vm.lastName,
          'id' : vm.id
        }
      );

      vm.firstName = '';
      vm.lastName = '';

      $scope.$broadcast('refreshAuthorList', {});
    }
  }

})();
