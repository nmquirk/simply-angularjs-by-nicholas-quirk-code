(function () {
	'use strict';

	angular
		.module ('app.author')
		.component ('viewAuthors', {

		//
		// Usage: <add-edit-author></add-edit-author>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName">
				<table>
					<thead>
						<th>actions</th>
						<th>first name</th>
						<th>last name</th>
					</thead>
					<tbody>
						<tr ng-repeat="author in vm.authors track by $index">
							<td>
								<button ng-click="vm.editAuthor(author)">edit</button>
								<button ng-click="vm.deleteAuthor(author)" class="error">delete</button>
							</td>
							<td>{{author.firstName}}</td>
							<td>{{author.lastName}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			`,
			controller: 'viewAuthorsController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<'
			},
			bindToController: true // because the scope is isolated
		})

})();
