(function(){

  'use strict';

	angular.module('app.author')
  .controller('viewAuthorsController', viewAuthorsController)

	viewAuthorsController.$inject = ['$scope', 'dataservice'];

	function viewAuthorsController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'author';
    vm.authors = [];

    // function declarations
    vm.onInit = onInit;
    vm.editAuthor = editAuthor;
    vm.deleteAuthor = deleteAuthor;

    // event handlers
    $scope.$on('refreshAuthorList', function(event, data) {
      onInit();
    });

    // function implementations
    function editAuthor(author) {
      $scope.$emit('editAuthor', author);
    }

    function deleteAuthor(author) {
      dataservice.deleteAuthor(author);
      onInit();
    }

    function onInit() {
      vm.authors = dataservice.getAuthors();
    }

    onInit();
	}

})();
