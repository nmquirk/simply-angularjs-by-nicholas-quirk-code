(function(){

  'use strict';

	angular.module('app.vendor')
  .controller('addEditVendorController', addEditVendorController)

	addEditVendorController.$inject = ['$scope', 'dataservice'];

	function addEditVendorController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'vendor';


    // function declarations
    vm.save = save;

    // event handlers
    $scope.$on('editVendor', function(event, data) {
      vm.name = data.name;
      vm.id = data.id;
    });

    // function implementations
    function save() {
      dataservice.saveVendor(
        { 'name': vm.name,
          'id' : vm.id
        }
      );

      vm.name = '';

      $scope.$broadcast('refreshVendorList', {});
    }
	}

})();
