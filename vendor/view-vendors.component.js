(function () {
	'use strict';

	angular
		.module ('app.vendor')
		.component ('viewVendors', {

		//
		// Usage: <add-edit-author></add-edit-author>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName">
				<table>
					<thead>
						<th>actions</th>
						<th>name</th>
					</thead>
					<tbody>
						<tr ng-repeat="vendor in vm.vendors track by $index">
							<td>
								<button ng-click="vm.editVendor(vendor)">edit</button>
								<button ng-click="vm.deleteVendor(vendor)" class="error">delete</button>
							</td>
							<td>{{vendor.name}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			`,
			controller: 'viewVendorsController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<'
			},
			bindToController: true // because the scope is isolated
		})

})();
