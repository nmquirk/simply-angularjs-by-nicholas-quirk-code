(function(){

  'use strict';

	angular.module('app.vendor')
  .controller('viewVendorsController', viewVendorsController)

	viewVendorsController.$inject = ['$scope', 'dataservice'];

	function viewVendorsController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'vendor';
    vm.onInit = onInit;
    vm.vendors = [];

    // function declarations
    vm.editVendor = editVendor;
    vm.deleteVendor = deleteVendor;

    // event handlers
    $scope.$on('refreshVendorList', function(event, data) {
      onInit();
    });

    // function declarations
    function editVendor(vendor) {
      $scope.$emit('editVendor', vendor);
    }

    function deleteVendor(vendor) {
      dataservice.deleteVendor(vendor);
      onInit();
    }

    function onInit() {
      vm.vendors = dataservice.getVendors();
    }

    onInit();
	}

})();
