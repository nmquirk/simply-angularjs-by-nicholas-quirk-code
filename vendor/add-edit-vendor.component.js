(function () {
	'use strict';

	angular
		.module ('app.vendor')
		.component ('addEditVendor', {

		//
		// Usage: <add-edit-vendor></add-edit-vendor>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName"'>
				<form>
					<fieldset class="flex four">
						<input type="hidden" ng-model="vm.id"/>
						<label><input type="text" placeholder="name" ng-model="vm.name"/></label>
					</fieldset>
					<button ng-click="vm.save()" class="success">save</button>
				</form>
			</div>
			<view-vendors visible-component-name="vm.visibleComponentName"></view-vendors>
			`,
			controller: 'addEditVendorController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<' // make this 1-way
			},
			bindToController: true // because the scope is isolated
		})

})();
