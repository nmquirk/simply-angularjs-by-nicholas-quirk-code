(function(){

  'use strict';

	angular.module('app.book')
  .controller('addEditBookController', addEditBookController)

	addEditBookController.$inject = ['$scope', 'dataservice'];

	function addEditBookController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'book';

    vm.authors = [];

    // function declarations
    vm.onInit = onInit;
    vm.save = save;

    // event handlers
    $scope.$on('editBook', function(event, data) {
      vm.title = data.title;
      vm.isbn = data.isbn;
      vm.selectedAuthor = data.author;
      vm.id = data.id;
    });

    $scope.$on('componentRefresh', function(event, data) {
      onInit();
    });

    // function implementations
    function save() {
      dataservice.saveBook(
        { 'title': vm.title,
          'isbn' : vm.isbn,
          'author' : vm.selectedAuthor,
          'id' : vm.id
        }
      );

      vm.title = '';
      vm.isbn = '';
      vm.selectedAuthor = '';
      vm.id = '';

      $scope.$broadcast('refreshBookList', {});
    }

    function onInit() {
      vm.authors = dataservice.getAuthors();
    }

    onInit();
	}

})();
