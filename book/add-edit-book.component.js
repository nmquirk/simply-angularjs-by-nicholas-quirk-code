(function () {
	'use strict';

	angular
		.module ('app.book')
		.component ('addEditBook', {

		//
		// Usage: <add-edit-book></add-edit-book>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName"'>
				<form>
					<fieldset class="flex four">
						<input type="hidden" ng-model="vm.id"/>
						<label><input type="text" placeholder="title" ng-model="vm.title"/></label>
						<label><input type="text" placeholder="isbn" ng-model="vm.isbn"/></label>
						<label>
							<select ng-model="vm.selectedAuthor">
								<option value="">author</option>
								<option ng-repeat="author in vm.authors">{{author.firstName}} {{author.lastName}}</option>
							</select>
						</label>
					</fieldset>
					<button ng-click="vm.save()" class="success">save</button>
				</form>
			</div>
			<view-books visible-component-name="vm.visibleComponentName"></view-books>
			`,
			controller: 'addEditBookController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<' // make this 1-way
			},
			bindToController: true // because the scope is isolated
		})

})();
