(function(){

  'use strict';

	angular.module('app.book')
  .controller('viewBooksController', viewBooksController)

	viewBooksController.$inject = ['$scope', 'dataservice'];

	function viewBooksController($scope, dataservice) {

		let vm = this;

    // data declarations
    vm.componentName = 'book';
    vm.books = [];

    // function declarations
    vm.onInit = onInit;
    vm.editBook = editBook;
    vm.deleteBook = deleteBook;

    // event handlers
    $scope.$on('refreshBookList', function(event, data) {
      onInit();
    });

    // function implementations
    function editBook(book) {
      $scope.$emit('editBook', book);
    }

    function deleteBook(book) {
      dataservice.deleteBook(book);
      onInit();
    }

    function onInit() {
      vm.books = dataservice.getBooks();
    }

    onInit();
	}

})();
