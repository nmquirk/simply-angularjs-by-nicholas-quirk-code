(function () {
	'use strict';

	angular
		.module ('app.book')
		.component ('viewBooks', {

		//
		// Usage: <add-edit-author></add-edit-author>
		//
			restrict: 'EA',
			// If using a web-server use templateUrl.
			// Using templateUrl with localfiles will throw a CORS error
			//https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default
			//templateUrl: 'add-edit-author.html',
			template:
			`
			<div ng-show="vm.visibleComponentName == vm.componentName">
				<table>
					<thead>
						<th>actions</th>
						<th>title</th>
						<th>isbn</th>
						<th>author</th>
					</thead>
					<tbody>
						<tr ng-repeat="book in vm.books track by $index">
							<td>
								<button ng-click="vm.editBook(book)">edit</button>
								<button ng-click="vm.deleteBook(book)" class="error">delete</button>
							</td>
							<td>{{book.title}}</td>
							<td>{{book.isbn}}</td>
							<td>{{book.author}}</td>
						</tr>
					</tbody>
				</table>
			</div>
			`,
			controller: 'viewBooksController',
			controllerAs: 'vm',
			bindings: {
				visibleComponentName : '<'
			},
			bindToController: true // because the scope is isolated
		})

})();
